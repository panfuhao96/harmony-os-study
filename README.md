# HarmonyOS Study

## 介绍

鸿蒙基于 arkts 基于声明式开发的 arkui 组件的示范记录。

💻 开发软件‍ : DevEco3.1.0.501 

![hmos](/.image/node.png) node.js版本 : v16.19.1

![hmos](/.image/hmos.ico) HarmonyOsSdk : 3.1.0 **(API9)**

![hmos](/.image/hmos.ico) HarmonyOs组件文档：<https://developer.harmonyos.com/cn/docs/documentation/doc-references-V3/ts-components-summary-0000001478181369-V3>

---

## ArkUI 布局

| 是否完成 | 布局方式                  | 💬 描述                         | 📄 文件                                                                             | 👁️‍🗨️ 预览                                                          |
|:-----|-----------------------|-------------------------------|-----------------------------------------------------------------------------------|:--------------------------------------------------------------------|
| ✅    | 线性布局Row/Column        | 根据官网教程编写线性布局Row/Column        | [xxbj.ets](entry/src/main/ets/pages/xxbj.ets)                                     | ![线性布局](/.image/线性布局.gif)                                           |
| ✅    | 层叠布局Stack             | 根据官网教程编写层叠布局Stack             | [StackStudy.ets](entry/src/main/ets/pages/StackStudy.ets)                         | ![层叠布局Stack](/.image/层叠布局Stack.gif)                                 |
| ✅    | 弹性布局Flex              | 根据官网教程编写弹性布局Flex              | [FlexStudy.ets](entry/src/main/ets/pages/FlexStudy.ets)                           | ![弹性布局Flex](/.image/弹性布局Flex.gif)                                   |
| ✅    | 相对布局RelativeContainer | 根据官网教程编写相对布局RelativeContainer | [RelativeContainerStudy.ets](entry/src/main/ets/pages/RelativeContainerStudy.ets) | ![根据官网教程编写相对布局RelativeContainer](/.image/相对布局RelativeContainer.gif) |
| ❌    | 栅格布局（GridRow/GridCol） | 根据官网教程编写栅格布局（GridRow/GridCol） |                                                                                   |                                                                     |